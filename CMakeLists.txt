## Preamble

cmake_minimum_required(VERSION 3.21)

include( project-meta.in )

project(bbe
        VERSION ${project_version}
        DESCRIPTION "bbe : the binary block editor"
        HOMEPAGE_URL "https://gitlab.com/fredrick.eisele/bbe"
        LANGUAGES C)
enable_testing()

## Project wide setup

set_property( GLOBAL PROPERTY USE_FOLDERS YES )

list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake/Modules")
set(CMAKE_C_STANDARD 99)
set(CMAKE_C_STANDARD_REQUIRED YES)
include(pandoc)

## Dependencies
add_subdirectory( dependencies )

## Main build targets

add_subdirectory( app )

## Tests & Packaging
if(CMAKE_SOURCE_DIR STREQUAL CMAKE_CURRENT_SOURCE_DIR)
    add_subdirectory(tests)
    add_subdirectory(packaging)
endif()

