import os
'''
https://docs.conan.io/en/latest/reference/conanfile/attributes.html#generators
'''

from conans import ConanFile, CMake, tools


class BbeConan(ConanFile):
    name = "bbe"
    version = "0.3.0"
    license = "Apache"
    author = "Fred Eisele <fredrick.eisele@gmail.com>"
    url = "https://github.com/babeloff/bbe"
    description = "bbe - binary block editor"
    topics = ("binary", "stream", "editor")
    settings = "os", "compiler", "build_type", "arch"
    requires = (
        # "gnulib/20200224"
        "pcre2/10.39"
        # "hyperscan/5.4.0"
        # "oniguruma/6.9.7.1"
    )
    default_options = {}
    generators =  ["cmake_find_package","cmake_paths"]
    source_folder = "src"


    def configure(self):
        del self.settings.compiler.libcxx
        del self.settings.compiler.cppstd

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()
